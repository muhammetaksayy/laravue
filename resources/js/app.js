// import 'es6-promise/auto'
// import axios from 'axios'
import Vue from 'vue'
// import Vuex from 'vuex'
// import { store } from './store.js'
// import VueAuth from '@websanova/vue-auth'
// import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import App from './App.vue'
// import auth from './auth'
import router from './router'
// import Buefy from 'buefy'
// import '@fortawesome/fontawesome-free/css/all.css'
// import '@fortawesome/fontawesome-free/js/all.js'
// import IdleVue from "idle-vue";
// import Vuelidate from 'vuelidate'

// Set Vue globally
window.Vue = Vue

// Vue.use(Vuex)

// Set Vue router
Vue.router = router
Vue.use(VueRouter)
// Vue.use(Vuelidate)

// Set Vue authentication
// Vue.use(VueAxios, axios)
// axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api`
// axios.defaults.baseURL = `/api`

// Vue.use(VueAuth, auth)

// Vue.use(Buefy)
// Vue.use(Buefy, {
//  defaultIconPack: 'fas',
//  defaultFirstDayOfWeek: 1,
  // defaultDayNames: ['Pa', 'Pz', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
  // defaultMonthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
  // defaultDateFormatter: date => {
  //  return moment(date).format("DD.MM.YYYY");
  // },
  // defaultDateParser: date => {
  //  return moment(date, "DD.MM.YYYY").toDate();
  // }
// })

// const eventsHub = new Vue();

// Vue.use(IdleVue, {
//   eventEmitter: eventsHub,
//   store,
//   idleTime: 300000, // 5 dakika hareketsizlik olursa logout yapar.
//   startAtIdle: false
// });

// Load Index
Vue.component('app', App)

const app = new Vue({
  el: '#app',
  router
  // store
});
