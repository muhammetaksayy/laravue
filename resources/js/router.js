import VueRouter from 'vue-router'

// Pages
import Test from './pages/Test.vue'

// Routes
const routes = [
  { path: '/', name: 'homepage', component: Test },
  { path: '/test', name: 'testpage', component: Test },
]

const router = new VueRouter({
  history: true,
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes,
})

export default router
