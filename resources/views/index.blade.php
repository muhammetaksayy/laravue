<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" translate="no">
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="{{ csrf_token() }}" name="csrf-token" />
        <title>{{ config('app.name') }}</title>
        <script defer="" src="{{ mix('/js/app.js') }}"></script>
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet" />
    </head>
    <body>
        <div id="app">
            <app />
        </div>
    </body>
</html>
